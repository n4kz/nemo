(function () {
	'use strict';

	/*jslint es5:true white:true browser:true bitwise:true plusplus:true */
	/*global Stalin:false */
	/*global Node:false */

	var handlers, groups,
		container    = document.createElement('div'),
		activeGroup  = null,
		activeSquare = null,
		classNames   = {
			groups: 'b-groups',
			group: 'b-group',
			row: 'b-row',
			square: 'b-square',
			cursor: 'cursor',
			active: 'active'
		};

	/* Render template and append to node */
	function append (node, template, data) {
		container.innerHTML = Stalin[template](data);
		node.appendChild(container.childNodes[0]);
	}

	/* Returns true if node has class */
	function has (element, classname) {
		return element.classList.contains(classname);
	}

	/* Find closest parent by class name */
	function closest (element, classname) {
		var node = element;

		do {
			if (has(node, classname)) {
				break;
			}

			node = node.parentNode;

			if (node === document) {
				return undefined;
			}
		} while (node);

		return node;
	}

	/* Get node children by class name */
	function children (element, classname) {
		var nodes  = element.childNodes,
			result = [],
			i, l, node;

		for (i = 0, l = nodes.length; i < l; i++) {
			node = nodes[i];

			if (node.nodeType === Node.ELEMENT_NODE) {
				if (has(node, classname)) {
					result.push(node);
				}
			}
		}

		return result;
	}

	/* Select first square in active group */
	function selectFirst () {
		if (activeGroup) {
			activeSquare = children(children(activeGroup, classNames.row)[0], classNames.square)[0];
			activeSquare.classList.add(classNames.cursor);
		} else {
			activeSquare = null;
		}
	}

	/* Handler for all clicks */
	function clickHandler (event) {
		var target = event.target,
			action = target.getAttribute('data-action'),
			type, group, i, l, nodes;

		if (action) {
			action = action.split(':');
			handlers[action[0]][action[1]](target);
		} else {
			group = closest(target, classNames.group);
			nodes = document.getElementsByClassName(classNames.group);

			for (i = 0, l = nodes.length; i < l; i++) {
				nodes[i].classList.remove(classNames.active);
			}

			if (group) {
				group.classList.add(classNames.active);

				if (activeGroup !== group) {
					if (activeSquare) {
						activeSquare.classList.remove(classNames.cursor);
						activeSquare = null;
					}
				}

				activeGroup = group;

				if (!activeSquare) {
					selectFirst();
				}
			} else if (activeSquare) {
				activeSquare.classList.remove(classNames.cursor);
				activeSquare = null;
			}
		}

		event.preventDefault();
	}

	/* Keypress handler */
	function keyHandler (event) {
		var row, rows, squares, i, l, nodes, rowid, squareid;

		activeGroup = activeGroup || children(groups, classNames.group)[0] || null;

		if (!activeGroup) {
			return;
		}

		if (!activeSquare) {
			selectFirst();
		}

		row = closest(activeSquare, classNames.row);
		rows = children(activeGroup, classNames.row);

		squares = children(row, classNames.square);

		for (i = 0, l = rows.length; i < l; i++) {
			if (row === rows[i]) {
				rowid = i;
				break;
			}
		}

		for (i = 0, l = squares.length; i < l; i++) {
			if (activeSquare === squares[i]) {
				squareid = i;
				break;
			}
		}

		activeSquare.classList.remove(classNames.cursor);

		switch (event.keyCode) {
			/* Left */
			case 37:
				activeSquare = squares[squareid? squareid - 1 : 0];
				break;

			/* Right */
			case 39:
				if (++squareid < squares.length) {
					activeSquare = squares[squareid];
				}
				break;

			/* Up */
			case 38:
				if (rowid) {
					squares = children(rows[--rowid], classNames.square);
					activeSquare = squares[squareid] || squares[squares.length - 1];
				}
				break;

			/* Down */
			case 40:
				if (++rowid < rows.length) {
					squares = children(rows[rowid], classNames.square);
					activeSquare = squares[squareid] || squares[squares.length - 1];
				}
				break;
		}

		if (~[37, 38, 39, 40].indexOf(event.keyCode)) {
			event.preventDefault();
		}

		if (activeSquare) {
			activeSquare.classList.add(classNames.cursor);
		}
	}

	/* Action handlers */
	handlers = {
		group: {
			add: function (element) {
				append(groups, 'group');
			},

			del: function (element) {
				var group = closest(element, classNames.group);

				if (activeGroup === group) {
					activeGroup = null;
				}

				group.remove();
			}
		},

		row: {
			add: function (element) {
				append(closest(element, classNames.group), 'row');
			}
		},

		square: {
			add: function (element) {
				append(closest(element, classNames.row), 'square');
			},

			del: function (element) {
				var square = closest(element, classNames.square),
					row    = closest(square, classNames.row),
					group  = closest(row, classNames.group);

				if (activeSquare === square) {
					activeSquare = null;
				}

				square.remove();

				/* Row is empty now */
				if (!children(row, classNames.square).length) {
					row.remove();
				}

				/* Group is empty now */
				if (!children(group, classNames.row).length) {
					group.remove();

					if (activeGroup === group) {
						activeGroup = null;
					}
				}

				if (!activeSquare) {
					selectFirst();
				}
			}
		}
	};

	document.addEventListener('readystatechange', function () {
		var scripts, i, l, template;

		if (document.readyState !== 'complete') {
			return;
		}

		/* Get container for groups */
		groups = document.getElementsByClassName(classNames.groups)[0];

		/* Add click handler */
		document.addEventListener('click', clickHandler);

		/* Add keypress handler */
		document.addEventListener('keydown', keyHandler);
	});
}());
